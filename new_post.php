<?php include('include/header.php'); ?>

<?php
if ($_POST['submit-post']) {


    $title = $_POST['title'];
    $description = $_POST['description'];
    $date = date("Y-m-d");
    $cat_id = $_POST['cat_id'];
    $c_like = 0;


    /*

      จัดการไฟล์

     */

    $UploadDirectory = 'images/post/';  // ตั้ง directory เก็บไฟล์ที่อัพโหลด ตรวจสอบ / ให้ดี  

    if (!@file_exists($UploadDirectory)) {
        //ไม่มี directory ที่ตั้งไว้

        die("กรุณาตรวจสอบ Directory ที่ตั้งไว้ว่าถูกต้องมั้ย");
    }



    $FileName = strtolower($_FILES['filUpload']['name']); // ชื่อรูปภาพที่อัพโหลดเข้ามา ทำให้เป็นตัวพิมพ์เล็กทั้งหมดด้วยฟังก์ชั่น php : strtolower()
    $FileType = $_FILES['filUpload']['type']; //file type
    $FileSize = $_FILES['filUpload']["size"]; //file size

    if (move_uploaded_file($_FILES["filUpload"]["tmp_name"], $UploadDirectory . $_FILES["filUpload"]["name"])) {
        //connect & insert file record in database

        $sql_insert = "INSERT INTO  post (
      `pt_id` ,
      `user_id` ,
      `cat_id` ,
      `title` ,
      `description` ,
      `date` ,
      `c_like` ,
      `img`
      )
      VALUES (
      NULL,  '" . $_SESSION["user_id"] . "',  '" . $cat_id . "',  '" . $title . "',  '" . $description . "',  '" . $date . "',  '" . $c_like . "',  '" . $FileName . "'
      );
      ";

        $rs_insert = mysql_query($sql_insert);

        if($rs_insert) {
            ?>
            <script type="text/javascript">
                alert('การเพิ่มโพสต์ใหม่สำเร็จ..');
                window.location.href = 'all_post.php';
                exit();
            </script>

            <?php
        } else {
            ?>
            <script type="text/javascript">
                alert('การเพิ่มข้อมูลผิดพลาด กรุณาตรวจสอบ');
                window.location.href = 'new_post.php';
                exit();
            </script>

            <?php
        }

    } else {
        ?>
            <script type="text/javascript">
                alert('การอัพโหลดรูปผิดพลาด กรุณาตรวจสอบ');
                window.location.href = 'new_post.php';
                exit();
            </script>

            <?php
    }
}
?>



<div id="main">

    <div class="row">
        <div id="side-bar" class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
<?php include("include/side-bar.php"); ?>
        </div>
        <div id="content"  class="col-xs-9 col-sm-9 col-md-9 col-lg-9">
            <div class="well">
                <div class="panel panel-success">
                    <div class="panel-heading">
                        <h3 class="panel-title"> <span class="glyphicon glyphicon-plus" style="margin-right: 10px;"></span>เพิ่มโพสต์ใหม่</h3>
                    </div>
                    <div class="panel-body">

                        <div class="post">
                            <form id="post-form" action="new_post.php" method="post" enctype="multipart/form-data">
                                <div style="padding-bottom: 15px;">
                                    <h3>Title</h3>
                                </div>
                                <div class="form-group">
                                    <input type="text" class="form-control" name="title" placeholder="ชื่อเรื่อง">
                                </div>
                                <div style="padding-bottom: 15px;">
                                    <h3>Description</h3>
                                </div>
                                <div class="form-group">
                                    <textarea class="form-control" rows="10" name="description" placeholder="เนื้อหา"></textarea>
                                </div>
                                <div class="form-group">
                                    <div style="padding-bottom: 15px;">
                                        <h3>Category</h3>
                                    </div>
                                    <select class="form-control" name="cat_id">
                                        <option value="*">กรุณาเลือกประเภทโพสต์</option>
<?php
$sql = "select * from category ";
$result = mysql_query($sql);

while ($rs2 = mysql_fetch_array($result)) {
    ?>
                                            <option    value="<?= $rs2['cat_id']; ?>"><?= $rs2['cat_name']; ?></option>
                                            <?php
                                        }
                                        ?>
                                    </select>  
                                </div>
                                <div style="padding-bottom: 15px;">
                                    <h3>Image</h3>
                                </div>
                                <div class="upload">
                                    <div class="fileupload fileupload-new" data-provides="fileupload">
                                        <div class="fileupload-preview thumbnail" style="width: 200px; height: 150px;"></div>
                                        <div>
                                            <span class="btn btn-default btn-file"><span class="fileupload-new">Select image</span><span class="fileupload-exists">Change</span><input name="filUpload" type="file"></span>
                                            <a href="#" class="btn fileupload-exists" data-dismiss="fileupload">Remove</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group" style="width: 150px;margin: 0 auto;">
                                    <input type="submit" name="submit-post" class="btn btn-warning"  value="เพิ่มโพสต์ใหม่" />
                                </div>
                            </form>
                        </div>

                    </div>
                </div>
            </div>
        </div>

    </div>
</div>





<?php include('include/footer.php'); ?>