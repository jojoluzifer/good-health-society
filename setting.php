<?php include('include/header.php'); ?>


<?php

//select ข้อมูลผู้ใช้จาก session id

$user_id = $_SESSION['user_id'];


$sql_select = "select * from users where user_id = '$user_id'";

$result = mysql_query($sql_select);
if (!$result) {

    die('การคิวรี่ผิดพลาด : ' . mysql_error());
} else {
    while ($rs = mysql_fetch_array($result)) {

        $utype_id = $rs['utype_id'];
        $username = $rs['username'];
        $password = $rs['password'];
        $first_name = $rs['first_name'];
        $last_name = $rs['last_name'];
        $email = $rs['email'];
        $birth_day = $rs['birth_day'];
        $sex = $rs['sex'];
        $address = $rs['address'];
        $tel = $rs['tel'];
    }
}

?>

<div id="main">

    <div class="row">
        <div id="side-bar" class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
<?php include("include/side-bar.php"); ?>
        </div>
        <div id="content"  class="col-xs-9 col-sm-9 col-md-9 col-lg-9">
            <div class="well">
                <div class="sign-up">

                    <form id="sign-up-form">


                        <div class="user">
                            <div class="panel panel-success">
                                <div class="panel-heading">
                                    <h3 class="panel-title">ข้อมูล Account</h3>
                                </div>
                                <div class="panel-body">
                                    <div class="row">
                                        <div  class="col-xs-3 col-sm-3 col-md-3 ">
                                            <label class="text">Username :</label>
                                        </div>
                                        <div  class="col-xs-4 col-sm-4 col-md-4 col-lg-4 col-xs-offset-1 col-sm-offset-1col-md-offset-1 col-lg-offset-1">
                                            <input type="text" name="username" class="form-control" placeholder="Username" value="<?php echo $user_id ? $username : ""; ?>" disabled>
                                        </div>

                                    </div>

                                    <div class="row">
                                        <div  class="col-xs-3 col-sm-3 col-md-3 col-lg-3 ">
                                            <label class="text">Password :</label>
                                        </div>
                                        <div  class="col-xs-4 col-sm-4 col-md-4 col-lg-4 col-xs-offset-1 col-sm-offset-1col-md-offset-1 col-lg-offset-1">
                                            <input type="password" id="password"    name="password" class="form-control" value="<?php echo $user_id ? $password : ""; ?>" placeholder="Password" disabled>
                                        </div>

                                    </div>


                                </div>
                            </div>
                            <div class="panel panel-info">
                                <div class="panel-heading">
                                    <h3 class="panel-title">ข้อมูล Profile</h3>
                                </div>
                                <div class="panel-body">
                                    <div class="row">
                                        <div  class="col-xs-3 col-sm-3 col-md-3 ">
                                            <label class="text">ชื่อจริง :</label>
                                        </div>
                                        <div  class="col-xs-5 col-sm-5 col-md-5 col-lg-5 col-xs-offset-1 col-sm-offset-1col-md-offset-1 col-lg-offset-1">
                                            <input type="text" name="first_name"  class="form-control" value="<?php echo $user_id ? $first_name : ""; ?>" placeholder="ชื่อจริง">
                                        </div>

                                    </div>

                                    <div class="row">
                                        <div  class="col-xs-3 col-sm-3 col-md-3 col-lg-3 ">
                                            <label class="text">นามสกุล :</label>
                                        </div>
                                        <div  class="col-xs-5 col-sm-5 col-md-5 col-lg-5 col-xs-offset-1 col-sm-offset-1col-md-offset-1 col-lg-offset-1">
                                            <input type="text" name="last_name" class="form-control" value="<?php echo $user_id ? $last_name : ""; ?>" placeholder="นามสกุล">
                                        </div>

                                    </div>
                                    <div class="row">
                                        <div  class="col-xs-3 col-sm-3 col-md-3 col-lg-3 ">
                                            <label class="text">เพศ :</label>
                                        </div>
                                        <div  class="col-xs-5 col-sm-5 col-md-5 col-lg-5 col-xs-offset-1 col-sm-offset-1col-md-offset-1 col-lg-offset-1">

                                            <div class="row">
                                                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 ">
                                                    <input type="radio" name="sex" id="optionsRadios1" value="1"
<?php
if (!empty($user_id) && $sex == 1) {
    echo "checked";
}
?>

                                                           > ผู้ชาย
                                                </div>
                                                <div class="col-xs-6 col-sm-6 col-md-6 col-lg-6 ">
                                                    <input type="radio" name="sex" id="optionsRadios1" value="2" 
<?php
if (!empty($user_id) && $sex == 2) {
    echo "checked";
}
?>
                                                           > ผู้หญิง
                                                </div>
                                            </div>

                                        </div>

                                    </div>

                                    <div class="row">
                                        <div  class="col-xs-3 col-sm-3 col-md-. col-lg-3">
                                            <label class="text">Email :</label>
                                        </div>
                                        <div  class="col-xs-5 col-sm-5 col-md-5 col-lg-5 col-xs-offset-1 col-sm-offset-1col-md-offset-1 col-lg-offset-1">
                                            <input type="email" name="email" class="form-control" value="<?php echo $user_id ? $email : ""; ?>"  placeholder="Email">
                                        </div>

                                    </div>
                                    <div class="row">
                                        <div  class="col-xs-3 col-sm-3 col-md-. col-lg-3">
                                            <label class="text">วันเกิด :</label>
                                        </div>
                                        <div  class="col-xs-5 col-sm-5 col-md-5 col-lg-5 col-xs-offset-1 col-sm-offset-1col-md-offset-1 col-lg-offset-1">
                                            <input type="text" name="birth_day" class="form-control datepicker" value="<?php echo $user_id ? $birth_day : ""; ?>" placeholder="วันเกิด (วัน-เดือน-ปี)">
                                        </div>

                                    </div>
                                    <script>
                                        $(document).ready(function() {

                                            $(".datepicker").datepicker({format: "dd-mm-yyyy"});

                                        });




                                    </script>
                                    <div class="row">
                                        <div  class="col-xs-3 col-sm-3 col-md-. col-lg-3">
                                            <label class="text">ที่อยู่ :</label>
                                        </div>
                                        <div  class="col-xs-5 col-sm-5 col-md-5 col-lg-5 col-xs-offset-1 col-sm-offset-1col-md-offset-1 col-lg-offset-1">
                                            <textarea name="address" class="form-control"  rows="4"><?php echo $user_id ? $address : ""; ?></textarea>
                                        </div>

                                    </div>
                                    <div class="row">
                                        <div  class="col-xs-3 col-sm-3 col-md-. col-lg-3">
                                            <label class="text">เบอร์โทร :</label>
                                        </div>
                                        <div  class="col-xs-5 col-sm-5 col-md-5 col-lg-5 col-xs-offset-1 col-sm-offset-1col-md-offset-1 col-lg-offset-1">
                                            <input name="tel" type="text" class="form-control" value="<?php echo $user_id ? $tel : ""; ?>"  placeholder="เบอร์โทร">
                                        </div>

                                    </div>


                                </div>
                            </div>
                            <div class="button">
                                <div class="btn-signUP">
                                    <input type="submit" class="btn btn-warning btn-lg" value="แก้ไขข้อมูล" />
                                </div>
                            </div>
                        </div>

                    </form>     

                </div>
            </div>
        </div>

    </div>
</div>





<?php include('include/footer.php'); ?>