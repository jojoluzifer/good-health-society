<?php include('include/header.php'); ?>

<div id="main">

    <div class="row">
        <div id="side-bar" class="col-xs-3 col-sm-3 col-md-3 col-lg-3">
            <?php include("include/side-bar.php"); ?>
        </div>
        <div id="content"  class="col-xs-9 col-sm-9 col-md-9 col-lg-9">
            <ul class="nav nav-tabs nav-justified" id="myTab">
                <!--                <li class="active"><a href="#activity" data-toggle="tab">กิจกรรม</a></li>-->
                <li class="active"><a href="#all-post" data-toggle="tab">โพสต์ทั้งหมด</a></li>
                <li><a href="#weight-loss" data-toggle="tab">ลดน้ำหนัก</a></li>
                <li><a href="#healthy-food" data-toggle="tab">อาหารเพื่อสุขภาพ</a></li>
                <li><a href="#exercise" data-toggle="tab">ออกกำลังกาย</a></li>
                <li><a href="#beaty" data-toggle="tab">ความสวยความงาม</a></li>
            </ul>
            <div class="tab-content">
                <div class="tab-pane fade in active" id="all-post">

                    <?php
                    $sql_post = "select * from post order by c_like desc ;";

                    $rs = mysql_query($sql_post);
                    
                    $chk_top_like = 0;
                    
                    
                    //จำนวนที่จะแสดงท๊อป ไลค์
                    $show_top = 1 ;
                    
                    while ($row = mysql_fetch_assoc($rs)) {
                        ?>
                    
                       <!-- เช็คจำนวน like เยอะสุด 3 อัน  (น้อยกว่า 4 นับจาก 1 คือเท่ากับ 3)  -->
                    
                        <div class='row <?php echo $chk_top_like < $show_top? "post-top-like" : ""; ?>' style="<?php echo $chk_top_like == $show_top - 1? "margin-bottom:20px;" : ""; ?>" >
                            
                            <?php
                            if($chk_top_like < $show_top){
                            ?>
                            <div class="top-ribbon">
                                <div class="layout">
                                    <p class="amount-like"><?= $row['c_like'] ?></p>
                                    <p class="text-like">Like</p>
                                </div>
                            </div>
                            <?php
                            }
                            ?>
                            
                            <div class='col-xs-2 col-sm-2 col-md-2 col-lg-2'>
                                <div class='all-post-img'>
                                    <a href="single_post.php?pt_id=<?= $row['pt_id'] ?>"> <img src='images/post/<?= $row["img"] ?>' class="img-thumbnail" /></a>
                                </div>
                            </div>
                            <div class='col-xs-8 col-sm-8 col-md-8 col-lg-8'>
                                <div class='all-post-title'>
                                    <a href='single_post.php?pt_id=<?= $row['pt_id'] ?>'><span class="glyphicon glyphicon-pencil" style='margin-right: 10px;'></span><?= $row["title"] ?></a>
                                </div>
                                <div class='all-post-content'>
                                    <label>
                                        <?php
                                        
                                       
                                            if(strlen($row["description"]) > 500 ){
                                                
                                                echo iconv_substr($row["description"],0,500, "UTF-8")."...";
                                                
                                            }else{
                                                echo $row["description"]."..." ;
                                            }
                                            
                                        ?>
                                    </label>
                                </div>
                                <div class='text-right'>
                                    <a href='single_post.php?pt_id=<?= $row['pt_id'] ?>' style='text-decoration: underline;color: red;font-size: 10px;' >อ่านต่อ</a>
                                </div>
                            </div>
                        </div>
                    <?php echo $chk_top_like < $show_top ? "" : "<hr/>"; ?>

                    
                        <?php
                        
                        $chk_top_like++ ;
                    }
                    ?>
                </div>
                <div class="tab-pane fade" id="weight-loss">
                </div>
                <div class="tab-pane fade" id="healthy-food">

                </div>
                <div class="tab-pane fade" id="exercise">
                </div>
                <div class="tab-pane fade" id="beaty">
                </div>
            </div>
        </div>

    </div>
</div>





<?php include('include/footer.php'); ?>