
<div class="col-xs-2 col-sm-2 col-md-2 col-lg-2" id="side-menu">

    <div class="admin-logo">
        <div class="img">
            <img src="images/logo.png" />
        </div>
        <div class="admin-menu">
            <div class="list-group">
                <a href="users.php" class="list-group-item">
                    ระบบผู้ใช้งาน
                    <span class="glyphicon glyphicon-user" style='float: right'> </span>               
                </a>
                <a href="#" class="list-group-item">
                    ระบบโพสต์
                    <span class="glyphicon glyphicon-file" style='float: right'> </span>               
                </a>
                <a href="#" class="list-group-item">
                    ระบบคอมเม้นท์
                    <span class="glyphicon glyphicon-comment" style='float: right'></span>  
                </a>
                <a href="#" class="list-group-item">
                    ระบบประเภทผู้ใช้
                    <span class="glyphicon glyphicon-star" style='float: right'></span>  
                </a>
                <a href="#" class="list-group-item">
                    ระบบหมวดหมู่ของโพสต์
                    <span class="glyphicon glyphicon-star" style='float: right'></span>  
                </a>
            </div>
        </div>
    </div>

</div>