   
<!--  ตรวจสอบการล็อกอินจากค่า Session ถ้าหากมีค่า Session ของ user_id แสดงว่ามีการล็อกอินแล้ว ให้ใช้ sidebar คนละแบบกัน  -->

<?php if (empty($_SESSION["user_id"])) { ?>

    <div class="sign-in">
        <div class="si-form">
            <form id="sign-in-form" action="index.php" method="post">
                <div class="head" >
                    <label class="title" >
                        LOGIN
                    </label>
                </div>
                <div style="border-left: 1px solid #ededed;border-right: 1px solid #ededed;">
                    <div class="main">
                        <div class="input-group">
                            <span class="input-group-addon"><span class="glyphicon glyphicon-user"></span></span>
                            <input type="text" class="form-control" name="username" placeholder="Username">
                        </div>
                        <div class="input-group">
                            <span class="input-group-addon"><span class="glyphicon glyphicon-lock"></span></span>
                            <input type="password" class="form-control" name="password" placeholder="Password">
                        </div>
                        <div class="input-group" style="display: block;text-align: center;margin-bottom: 0">
                            <a href="sign_up.php" style="text-decoration: underline;font-size: 12px;">สมัครสมาชิกใหม่</a>
                        </div>
                    </div>
                </div>
                <div class="button">
                    <div class="btn-signIN">

                        <input type="submit" name="submit-login" class="btn btn-danger btn-lg" value="เข้าสู่ระบบ" />
                    </div>
                </div>
            </form>
        </div>
    </div>


<?php } else { ?>

    <div class="sidebar-user">
        <div class="head">

            <div class="image">

                <img src="images/users/pang.jpg" class="img-circle" />
            </div>
            <div class="info">
                <label class="name">
                    Paeng Pawarana
                </label>
                <label class="follow">
                    150 Followers
                </label>
            </div>
        </div>
        <div class="main">
            <div class="menu">
                <div class="list-group">
                    <a href="setting.php" class="list-group-item">
                        ตั้งค่า
                        <span class="glyphicon glyphicon-cog" style='float: right'> </span>               
                    </a>
                    <a href="all_post.php" class="list-group-item">
                        โพสต์
                        <span class="glyphicon glyphicon-file" style='float: right'> </span>               
                    </a>
                    <a href="new_post.php" class="list-group-item">
                        เพิ่มโพสต์ใหม่
                        <span class="glyphicon glyphicon-plus" style='float: right'> </span>               
                    </a>
                    <a href="favorites.php" class="list-group-item">
                        เพื่อนคนโปรด
                        <span class="glyphicon glyphicon-heart" style='float: right'></span>  
                    </a>
                    
                </div>
            </div>
        </div>
    </div>


<?php } ?>




